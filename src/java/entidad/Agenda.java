/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "agenda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agenda.findAll", query = "SELECT a FROM Agenda a"),
    @NamedQuery(name = "Agenda.findByIdAgenda", query = "SELECT a FROM Agenda a WHERE a.idAgenda = :idAgenda"),
    @NamedQuery(name = "Agenda.findByConsultaF", query = "SELECT a FROM Agenda a WHERE a.fecha = :fecha  and a.tipo = :tipo"),
    @NamedQuery(name = "Agenda.findByTipo", query = "SELECT a FROM Agenda a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "Agenda.findByDuracion", query = "SELECT a FROM Agenda a WHERE a.duracion = :duracion"),
    @NamedQuery(name = "Agenda.findByNombre", query = "SELECT a FROM Agenda a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Agenda.findByFecha", query = "SELECT a FROM Agenda a WHERE a.fecha = :fecha"),
    @NamedQuery(name = "Agenda.findByHora", query = "SELECT a FROM Agenda a WHERE a.hora = :hora"),
    @NamedQuery(name = "Agenda.findByParticipantes", query = "SELECT a FROM Agenda a WHERE a.participantes = :participantes"),
    @NamedQuery(name = "Agenda.findByArea", query = "SELECT a FROM Agenda a WHERE a.area = :area"),
    @NamedQuery(name = "Agenda.findByFechaIncioReg", query = "SELECT a FROM Agenda a WHERE a.fechaIncioReg = :fechaIncioReg"),
    @NamedQuery(name = "Agenda.findByFechaFinReg", query = "SELECT a FROM Agenda a WHERE a.fechaFinReg = :fechaFinReg"),
    @NamedQuery(name = "Agenda.findByFechaAndLugar", query = "SELECT a FROM Agenda a WHERE a.fecha = :fecha and a.idLugares = :idlugares"),
    @NamedQuery(name = "Agenda.findByReunionEmp", query = "SELECT a FROM Agenda a, Reunionempleados r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idEmpleados.idEmpleados = :idEmpleados"),
    @NamedQuery(name = "Agenda.findByReunionProv", query = "SELECT a FROM Agenda a, Reunionproveedores r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idProveedores.idProveedores = :idProveedores"),
    
    @NamedQuery(name = "Agenda.findByReunionEmpNombre", query = "SELECT a FROM Agenda a, Reunionempleados r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idEmpleados.idEmpleados = :idProveedores AND a.nombre = :nombre "),
    @NamedQuery(name = "Agenda.findByReunionEmpFecha", query = "SELECT a FROM Agenda a, Reunionempleados r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idEmpleados.idEmpleados = :idProveedores AND a.fecha = :fecha "),
    @NamedQuery(name = "Agenda.findByReunionEmpHora", query = "SELECT a FROM Agenda a, Reunionempleados r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idEmpleados.idEmpleados = :idProveedores AND a.hora = :hora "),
    @NamedQuery(name = "Agenda.findByReunionEmpLugar", query = "SELECT a FROM Agenda a, Reunionempleados r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idEmpleados.idEmpleados = :idProveedores AND a.idLugares.nombre = :lugar "),
    @NamedQuery(name = "Agenda.findByReunionEmpNombreLugar", query = "SELECT a FROM Agenda a, Reunionempleados r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idEmpleados.idEmpleados = :idProveedores AND a.nombre = :nombre  AND a.idLugares.nombre = :lugar"),
    @NamedQuery(name = "Agenda.findByReunionEmpFechaHora", query = "SELECT a FROM Agenda a, Reunionempleados r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idEmpleados.idEmpleados = :idProveedores AND a.fecha = :fecha AND a.hora = :hora"),
    
    @NamedQuery(name = "Agenda.findByReunionProvNombre", query = "SELECT a FROM Agenda a, Reunionproveedores r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idProveedores.idProveedores = :idProveedores AND a.nombre = :nombre "),
    @NamedQuery(name = "Agenda.findByReunionProvFecha", query = "SELECT a FROM Agenda a, Reunionproveedores r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idProveedores.idProveedores = :idProveedores AND a.fecha = :fecha "),
    @NamedQuery(name = "Agenda.findByReunionProvHora", query = "SELECT a FROM Agenda a, Reunionproveedores r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idProveedores.idProveedores = :idProveedores AND a.hora = :hora "),
    @NamedQuery(name = "Agenda.findByReunionProvLugar", query = "SELECT a FROM Agenda a, Reunionproveedores r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idProveedores.idProveedores = :idProveedores AND a.idLugares.nombre = :lugar "),
    @NamedQuery(name = "Agenda.findByReunionProvNombreLugar", query = "SELECT a FROM Agenda a, Reunionproveedores r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idProveedores.idProveedores = :idProveedores AND a.nombre = :nombre AND a.idLugares.nombre = :lugar"),
    @NamedQuery(name = "Agenda.findByReunionProvFechaHora", query = "SELECT a FROM Agenda a, Reunionproveedores r WHERE a.idAgenda = r.idAgenda.idAgenda AND r.idProveedores.idProveedores = :idProveedores AND a.fecha = :fecha AND a.hora = :hora "),
        
})
public class Agenda implements Serializable {
    @JoinColumn(name = "idLugares", referencedColumnName = "idLugares")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Lugares idLugares;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAgenda")
    private Integer idAgenda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo")
    private int tipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "duracion")
    private int duracion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hora")
    @Temporal(TemporalType.TIME)
    private Date hora;
    @Basic(optional = false)
    @NotNull
    @Column(name = "participantes")
    private int participantes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "area")
    private String area;
    @Column(name = "FechaIncioReg")
    @Temporal(TemporalType.DATE)
    private Date fechaIncioReg;
    @Column(name = "FechaFinReg")
    @Temporal(TemporalType.DATE)
    private Date fechaFinReg;

    public Agenda() {
    }

    public Agenda(Integer idAgenda) {
        this.idAgenda = idAgenda;
    }

    public Agenda(Integer idAgenda, int tipo, int duracion, String nombre, Date fecha, Date hora, int participantes, String area) {
        this.idAgenda = idAgenda;
        this.tipo = tipo;
        this.duracion = duracion;
        this.nombre = nombre;
        this.fecha = fecha;
        this.hora = hora;
        this.participantes = participantes;
        this.area = area;
    }

    public Integer getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(Integer idAgenda) {
        this.idAgenda = idAgenda;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public int getParticipantes() {
        return participantes;
    }

    public void setParticipantes(int participantes) {
        this.participantes = participantes;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Date getFechaIncioReg() {
        return fechaIncioReg;
    }

    public void setFechaIncioReg(Date fechaIncioReg) {
        this.fechaIncioReg = fechaIncioReg;
    }

    public Date getFechaFinReg() {
        return fechaFinReg;
    }

    public void setFechaFinReg(Date fechaFinReg) {
        this.fechaFinReg = fechaFinReg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAgenda != null ? idAgenda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agenda)) {
            return false;
        }
        Agenda other = (Agenda) object;
        if ((this.idAgenda == null && other.idAgenda != null) || (this.idAgenda != null && !this.idAgenda.equals(other.idAgenda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Agenda[ idAgenda=" + idAgenda + " ]";
    }

    public Lugares getIdLugares() {
        return idLugares;
    }

    public void setIdLugares(Lugares idLugares) {
        this.idLugares = idLugares;
    }
    
}
