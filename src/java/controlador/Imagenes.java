/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Dianita
 */
@ManagedBean
@SessionScoped
public class Imagenes {

    private int foto;
    Random rn = new Random();

    public int getFoto() {

        int foto = rn.nextInt(9) + 1;
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

}
