package controlador;

import entidad.Empleados;
import controlador.util.JsfUtil;
import controlador.util.JsfUtil.PersistAction;
import sesion.EmpleadosFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.primefaces.model.DualListModel;

@ManagedBean(name = "empleadosController")
@ApplicationScoped
public class EmpleadosController implements Serializable {

    @EJB
    private sesion.EmpleadosFacade ejbFacade;
    private List<Empleados> items = null;
    private Empleados selected;
    
    @ManagedProperty("#{agendaController}")
    private AgendaController agenda;
    
    private DualListModel<Empleados> empleados;
    
    
    public EmpleadosController(){
          
    }
    @PostConstruct
    public void init()
    {
        System.out.println("#######################"+getAgenda()+"##########################");
        List<Empleados>empleadosSource = getFacade().findAllbyIdAgenda(getAgenda().getSelected());
        List<Empleados>empleadosTarget = new ArrayList<>();
        empleados = new DualListModel<>(empleadosSource, empleadosTarget);         
    }
    
    public DualListModel<Empleados> getEmpleados() {        
        //List<Empleados>empleadosSource = getFacade().findAllbyIdAgenda(getAgenda().getSelected());        
        //empleados = new DualListModel<>(empleadosSource, getEmpleadosTarget());
        return empleados;
    }
    
    public Empleados findByIdEmpleados(int idEmpleados){
        return getFacade().findByIdEmpleados(idEmpleados);
    }
    public void setEmpleados(DualListModel<Empleados> empleados) {
        //findAllbyIdAgenda();
        this.empleados = empleados;
    }

    public Empleados getSelected() {
        return selected;
    }

    public void setSelected(Empleados selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private EmpleadosFacade getFacade() {
        return ejbFacade;
    }

    public Empleados prepareCreate() {
        selected = new Empleados();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/agenda").getString("EmpleadosCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public String name(entidad.Usuarios user) {       
        List<Empleados> empleado=getFacade().findBySpecificField("idUsers", user,"equal", null, null);
        return empleado.get(0).getNombre();
    }
    

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/agenda").getString("EmpleadosUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/agenda").getString("EmpleadosDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Empleados> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Empleados> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Empleados> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    public AgendaController getAgenda() {
        return agenda;
    }

    public void setAgenda(AgendaController agenda) {
        this.agenda = agenda;
    }

   
    @FacesConverter(forClass = Empleados.class)
    public static class EmpleadosControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EmpleadosController controller = (EmpleadosController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "empleadosController");
            return controller.getFacade().find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Empleados) {
                Empleados o = (Empleados) object;
                return getStringKey(o.getIdEmpleados());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Empleados.class.getName()});
                return null;
            }
        }

    }

}
