package controlador;

import entidad.Agenda;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

@ManagedBean(name="dtMenuViewBean")
@ViewScoped
public class MenuViewBean {

    private String nombre;
    private Date fecha;
    private String lugar;
    private Date hora;    
    
    @ManagedProperty("#{agendaController}")
    private AgendaController agenda;
    
    private List<Agenda> lista;
    private List<SelectItem> nombres;
    private List<SelectItem> lugares;
    
    @PostConstruct
    public void init()
    {
         lista = agenda.findByReunionEmp(3);
         
         nombres = new ArrayList<>();
         for(Agenda item : lista)
         {
             nombres.add(new SelectItem(item.getNombre(),item.getNombre()));
         }
         
         lugares = new ArrayList<>();
         for(Agenda item : lista)
         {
             lugares.add(new SelectItem(item.getIdLugares().getNombre(),item.getIdLugares().getNombre()));
         }
    }
    
    
    public void setAgenda(AgendaController agenda) {
        this.agenda = agenda;
    }

    public List<Agenda> getLista() {
        return lista;
    }

    public void setLista(List<Agenda> lista) {
        this.lista = lista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }
    
    public void filterByNombreAndLugarAndEvento()
    {
        List<Agenda> tmp = lista;

        for (Agenda lista1 : lista) 
        {
            if(!nombre.equals("")&&lugar.equals(""))
            {                
                if(!lista1.getNombre().equals(nombre)&&lista1.getTipo()!=2)
                {
                    tmp.remove(lista1);
                }             
            }
            
            else if(nombre.equals("")&&!lugar.equals(""))
            {
                if(!lista1.getIdLugares().getNombre().equals(lugar)&&lista1.getTipo()!=2)
                {
                    tmp.remove(lista1);
                }
            }   
            
            else if(!nombre.equals("")&&!lugar.equals(""))
            {
                if(!lista1.getIdLugares().getNombre().equals(lugar)&&!lista1.getNombre().equals(nombre)&&lista1.getTipo()!=2)
                {
                    tmp.remove(lista1);
                }
            }
        }

        lista = tmp;        
    }
    
    public void filterByNombreAndLugarAndReunion()
    {
        lista = agenda.findByReunionEmp(3);  
        List<Agenda> tmp = new ArrayList<>();

        for (Agenda lista1 : lista) 
        {               
            if(lista1.getNombre().equals(nombre))
            {
                tmp.add(lista1);
            }
        }
        
        lista = tmp;    
    }


    public List<SelectItem> getNombres() {
        return nombres;
    }

    public void setNombres(List<SelectItem> nombres) {
        this.nombres = nombres;
    }

    public List<SelectItem> getLugares() {
        return lugares;
    }

    public void setLugares(List<SelectItem> lugares) {
        this.lugares = lugares;
    }
}