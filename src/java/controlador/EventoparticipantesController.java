package controlador;

import entidad.Eventoparticipantes;
import controlador.util.JsfUtil;
import controlador.util.JsfUtil.PersistAction;
import sesion.EventoparticipantesFacade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@ManagedBean(name = "eventoparticipantesController")
@SessionScoped
public class EventoparticipantesController implements Serializable {

    @EJB
    private sesion.EventoparticipantesFacade ejbFacade;
    private List<Eventoparticipantes> items = null;
    private List<Eventoparticipantes> items4 = null;
    private Eventoparticipantes selected;
    private List<Eventoparticipantes> items2 = null;
    private String message;
    private List<Eventoparticipantes> items3 = null;
    private List<Eventoparticipantes> selectedCars;
    private boolean isJuevesViernes = false, isViernes = false, isJueves = false;
    private Date hoy = new Date();

    public boolean isIsJuevesViernes() {
        isJuevesViernes = false;
        if (hoy.getDay() == 5 || hoy.getDay() == 4) {
            isJuevesViernes = true;
        }

        return isJuevesViernes;
    }

    public void setIsJuevesViernes(boolean isJuevesViernes) {
        this.isJuevesViernes = isJuevesViernes;
    }

    public boolean isIsViernes() {
        isViernes = false;
        if (hoy.getDay() == 5) {
            isViernes = true;
            System.out.println("hoy es viernes");
        }

        return isViernes;
    }

    public void setIsViernes(boolean isViernes) {
        this.isViernes = isViernes;
    }

    public boolean isIsJueves() {

        isJueves = false;
        if (hoy.getDay() == 4) {
            isJueves = true;
            System.out.println("hoy es jueves");
        }
        return isJueves;
    }

    public void setIsJueves(boolean isJueves) {
        this.isJueves = isJueves;
    }

    public EventoparticipantesController() {
    }

    public Eventoparticipantes getSelected() {
        return selected;
    }

    public void setSelected(Eventoparticipantes selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private EventoparticipantesFacade getFacade() {
        return ejbFacade;
    }

    public Eventoparticipantes prepareCreate() {
        selected = new Eventoparticipantes();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/agenda2").getString("EventoparticipantesCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/agenda2").getString("EventoparticipantesUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/agenda2").getString("EventoparticipantesDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Eventoparticipantes> getItems4(entidad.Agenda agenda) {
        try {

            System.out.println(agenda.getIdAgenda());
            items4 = getFacade().findByConfirmadoAgenda(true, agenda);

        } catch (NullPointerException e) {
        }

        return items4;
    }

    public List<Eventoparticipantes> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda2").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda2").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Eventoparticipantes> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Eventoparticipantes> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    /**
     * @param items2 the items2 to set
     */
    public void setItems2(List<Eventoparticipantes> items2) {
        this.items2 = items2;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the items3
     */
    public List<Eventoparticipantes> getItems3() {
        return items3;
    }

    /**
     * @param items3 the items3 to set
     */
    public void setItems3(List<Eventoparticipantes> items3) {
        System.out.println("primero");
        this.items3 = items3;
    }

    @FacesConverter(forClass = Eventoparticipantes.class)
    public static class EventoparticipantesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EventoparticipantesController controller = (EventoparticipantesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "eventoparticipantesController");
            return controller.getFacade().find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Eventoparticipantes) {
                Eventoparticipantes o = (Eventoparticipantes) object;
                return getStringKey(o.getIdEventoParticipantes());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Eventoparticipantes.class.getName()});
                return null;
            }
        }

    }

    public List<Eventoparticipantes> getItems2() {
        if (items == null) {
            items = getFacade().findAll();
        }

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getConfirmado() == false) {
                System.out.println("hola" + items.get(i));
                items2.add(items.get(i));
            }
        }

        return items2;
    }

    public void enviarInvitaciones() {
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Successful", "Invitaciones enviadas "));
        //context.addMessage(null, new FacesMessage("Second Message", "Additional Message Detail"));
    }

    public void confirmarInvitaciones() {
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Successful", "Invitaciones Confirmadas "));
        //context.addMessage(null, new FacesMessage("Second Message", "Additional Message Detail"));
    }

    public List<Eventoparticipantes> getSelectedCars() {
        return selectedCars;
    }

    public void setSelectedCars(List<Eventoparticipantes> selectedCars) {
        this.selectedCars = selectedCars;
    }

    public void eliminar() {
        System.out.println("akjdjka");
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Successful", "Confirmaciones eliminadas "));
        //context.addMessage(null, new FacesMessage("Second Message", "Additional Message Detail"));

        try {
            System.out.println(selectedCars.size());
            for (Eventoparticipantes items31 : selectedCars) {
                selected = items31;
                persist(PersistAction.DELETE, ResourceBundle.getBundle("/agenda2").getString("EventoparticipantesDeleted"));
                if (!JsfUtil.isValidationFailed()) {
                    selected = null; // Remove selection
                    items = null;    // Invalidate list of items to trigger re-query.
                }
            }
        } catch (NullPointerException e) {

        }
    }

}
