package controlador;

import entidad.Reunionproveedores;
import controlador.util.JsfUtil;
import controlador.util.JsfUtil.PersistAction;
import entidad.Eventoparticipantes;
import sesion.ReunionproveedoresFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.primefaces.model.DualListModel;

@ManagedBean(name = "reunionproveedoresController")
@SessionScoped
public class ReunionproveedoresController implements Serializable {

    @EJB
    private sesion.ReunionproveedoresFacade ejbFacade;
    private List<Reunionproveedores> items = null;
    private List<Reunionproveedores> items4 = null;
    private Reunionproveedores selected;

    public List<Reunionproveedores> getItems4(entidad.Agenda agenda) {
        try {

            System.out.println(agenda.getIdAgenda());
            items4 = getFacade().findByAgenda(agenda);

        } catch (NullPointerException e) {
        }

        return items4;
    }

    public ReunionproveedoresController() {
    }

    public Reunionproveedores getSelected() {
        return selected;
    }

    public void setSelected(Reunionproveedores selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ReunionproveedoresFacade getFacade() {
        return ejbFacade;
    }

    public Reunionproveedores prepareCreate() {
        selected = new Reunionproveedores();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/agenda2").getString("ReunionproveedoresCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/agenda2").getString("ReunionproveedoresUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/agenda2").getString("ReunionproveedoresDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Reunionproveedores> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }
    //======================PRUEBA DE PROVEEDORES==============================

//    private DualListModel<String> proveedores;
//
//    public DualListModel<String> getProveedores() {
//        return proveedores;
//    }
//    public void setProveedores(DualListModel<String> proveedores) {
//        this.proveedores = proveedores;
//    }
//
//    @PostConstruct
//    public void init() 
//    {
//        List<String> provedorSource = new ArrayList<String>();
//        List<String> provedorTarget = new ArrayList<String>();
//        
//        provedorSource.add("Pedro Aguilar");
//        provedorSource.add("Antonio Aguilar");
//        provedorSource.add("Cesar Alfredo");
//        provedorSource.add("Felipe Sánchez");
//        provedorSource.add("Juan Jiménez");
//        provedorSource.add("Teresa Teresa");
//        provedorSource.add("Lucio Luchador");
//         
//        proveedores = new DualListModel<String>(provedorSource, provedorTarget);
//         
//    }
//    
    //====================================================
    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda2").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda2").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Reunionproveedores> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Reunionproveedores> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Reunionproveedores.class)
    public static class ReunionproveedoresControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ReunionproveedoresController controller = (ReunionproveedoresController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "reunionproveedoresController");
            return controller.getFacade().find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Reunionproveedores) {
                Reunionproveedores o = (Reunionproveedores) object;
                return getStringKey(o.getIdReunionProveedores());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Reunionproveedores.class.getName()});
                return null;
            }
        }

    }

}
