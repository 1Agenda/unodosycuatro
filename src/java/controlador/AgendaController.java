package controlador;

import entidad.Agenda;
import controlador.util.JsfUtil;
import controlador.util.JsfUtil.PersistAction;
import entidad.Lugares;
import sesion.AgendaFacade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.primefaces.component.calendar.Calendar;

@ManagedBean(name = "agendaController", eager=true)
@ApplicationScoped
public class AgendaController implements Serializable {

    @EJB
    private sesion.AgendaFacade ejbFacade;
    private List<Agenda> items = null;
    private Agenda selected;
    private List<Agenda> items2 = null;
    private boolean isReunion = false;
    private boolean isEvento = false;
    private boolean isNnullx = false;
    private Agenda Eventos = null;
    
    public boolean isIsEvento() {
        isEvento = false;
        try {
            if ((selected.getTipo() == 1) && (selected.getIdAgenda() != -1)) {
                isEvento = true;
                isNnullx = true;
            }
        } catch (NullPointerException e) {
            isEvento = false;
            isNnullx = false;
        }
        return isEvento;
    }

    public void setIsEvento(boolean isEvento) {
        this.isEvento = isEvento;
    }

    public boolean isIsNnullx() {
        isNnullx = false;
        try {
            if ((selected.getIdAgenda() != -1)) {
                isNnullx = true;
            }
        } catch (NullPointerException e) {
            isReunion = false;
            isEvento = false;
            isNnullx = false;
        }

        return isNnullx;
    }

    public void setIsNnullx(boolean isNnullx) {
        this.isNnullx = isNnullx;
    }

    public boolean isIsReunion() {
        isReunion = false;
        try {
            if ((selected.getTipo() == 0) && (selected.getIdAgenda() != -1)) {
                isReunion = true;
                isNnullx = true;
            }
        } catch (NullPointerException e) {
            isReunion = false;
            isNnullx = false;
        }
        return isReunion;
    }

    public void setIsReunion(boolean isReunion) {
        this.isReunion = isReunion;
    }

    public AgendaController() {
    }

    public Agenda getSelected() {
        return selected;
    }

    public void setSelected(Agenda selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    public List<Agenda> findByFechaAndLugar(Calendar x, Lugares idLugares) {
        Date fecha = (Date) x.getValue();
        return getFacade().findByFechaAndLugar(fecha, idLugares);
    }

    protected void initializeEmbeddableKey() {
    }

    private AgendaFacade getFacade() {
        return ejbFacade;
    }

    public Agenda prepareCreate() {
        selected = new Agenda();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        if (selected.getTipo() == 1) {
            Date temp = addDaysToDate(selected.getFechaIncioReg(),2);
            selected.setFechaFinReg(temp);
        }
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/agenda3").getString("AgendaCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        if (selected.getTipo() == 1) {
            Date temp = addDaysToDate(selected.getFechaIncioReg(),2);
            selected.setFechaFinReg(temp);
        }
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/agenda3").getString("AgendaUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/agenda3").getString("AgendaDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Agenda> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    public List<Agenda> getItems2() {

        Date fecha = new Date ();
        Date consulta=new Date();
        
        int dia=fecha.getDay();
       int numero=0;
        switch(dia)
        {
            case 0:
                numero=8;
                break;
            case 1:
                numero=7;
                break;
            case 2:
                numero=6;
                break;
            case 3:
                numero=5;
                break;
            case 4:
                numero=4;
                break;
            case 5:
                numero=3;
                break;
            case 6:
                numero=2;
                break;                
            
        }
                
        if (items2 == null) {
            consulta=addDaysToDate(fecha,numero);
            items2 = getFacade().findByIndex(consulta);
        }

        return items2;
    }

    
    public List<Agenda> findByReunionEmp(int idEmpleados) {
        return getFacade().findByReunionEmp(idEmpleados);
    }

    public List<Agenda> findByReunionProv(int idProveedores) {
        return getFacade().findByReunionProv(idProveedores);
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda3").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda3").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Agenda> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Agenda> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Agenda.class)
    public static class AgendaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            AgendaController controller = (AgendaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "agendaController");
            return controller.getFacade().find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Agenda) {
                Agenda o = (Agenda) object;
                return getStringKey(o.getIdAgenda());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Agenda.class.getName()});
                return null;
            }
        }

    }

    public static Date addDaysToDate(Date date, int x) {
        java.util.Calendar calendarDate = java.util.Calendar.getInstance();
        calendarDate.setTime(date);
        calendarDate.add(java.util.Calendar.DAY_OF_YEAR, x);
        return calendarDate.getTime();
    }

}
