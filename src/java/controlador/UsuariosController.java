package controlador;

import entidad.Usuarios;
import controlador.util.JsfUtil;
import controlador.util.JsfUtil.PersistAction;
import entidad.Reunionempleados;
import entidad.Reunionproveedores;
import sesion.UsuariosFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import org.primefaces.context.RequestContext;
import sesion.ReunionempleadosFacade;

@ManagedBean(name = "usuariosController", eager = true)
@SessionScoped
public class UsuariosController implements Serializable {

    private List<Usuarios> items = new ArrayList();
    private List<Reunionempleados> items2 = new ArrayList();
    @Inject
    private UsuariosFacade ejbFacade;
    @Inject
    private sesion.ReunionempleadosFacade ejbFacade2;
    private Usuarios selected;
    private Reunionempleados selected2;

    @PostConstruct
    public void UsuariosController() {
        items = getFacade().findAll();
    }

    private String username;

    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public UsuariosFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String redirec;

    public String page() {
        if (selected != null) {
            if (selected.getNivel() == 1) {
                redirec = "vista_gerente?faces-redirect=true";
            } else if (selected.getNivel() == 2) {
                if (selected.getTipo() == 1) {
                    redirec = "vista_empleado?faces-redirect=true";
                } else {

                    if ((selected.getTipo() == 2)) {
                        redirec = "vista_proveedores?faces-redirect=true";
                    }
                }
            } else {
                if (selected.getNivel() == 0) {
                    Date fecha3 = new Date();
                    System.out.println("Imprimiendo la fecha " + fecha3.getDay());
                    if (fecha3.getDay() == 4) {
                        redirec = "Verificar_Jueves?faces-redirect=true";
                    } else {
                        if (fecha3.getDay() == 5) {
                            redirec = "verifica_viernes?faces-redirect=true";

                        } else {
                            redirec = "vista_Administrador?faces-redirect=true";
                        }

                    }
                }

            }

        }
        return redirec;
    }

    public void loginxw(ActionEvent event) {
        
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean loggedIn = false;
        items = getFacade().findByUserPasss(password, username);
        if (items != null) {
            loggedIn = true;
            selected = items.get(0);

        }

            if (loggedIn==false)
            {
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Loggin Error", "Invalid credentials");            
            }else
            {
               message=new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", "Bienvenido + username");
            }


        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", loggedIn);
    }

    public String login(Usuarios u) {

        System.out.println("SE OBTUVO EL USUARIO EN EL CONTROLLER");

        System.out.println(items.size());
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getUser() == u.getUser() && items.get(i).getPassword() == u.getPassword()) {
                int nivel = items.get(i).getNivel();
                if (nivel == 0) {
                    return "index";
                } else if (nivel == 1) {
                    return "template";
                } else if (nivel == 2) {
                    return "welcomePrimefaces";
                } else {
                    return "Intruso";
                }
            }

        }
        return null;
    }

    public UsuariosController() {
    }

    public Usuarios getSelected() {
        return selected;
    }

    public void setSelected(Usuarios selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private UsuariosFacade getFacade() {
        return ejbFacade;
    }

    private ReunionempleadosFacade getFacade2() {
        return ejbFacade2;
    }

    public List<Reunionempleados> getItems2() {
        if (items2 == null) {
            items2 = getFacade2().findAll();
        }
        return items2;
    }

    public Usuarios prepareCreate() {
        selected = new Usuarios();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/agenda").getString("UsuariosCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/agenda").getString("UsuariosUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/agenda").getString("UsuariosDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Usuarios> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Usuarios> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Usuarios> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Usuarios.class)
    public static class UsuariosControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UsuariosController controller = (UsuariosController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "usuariosController");
            return controller.getFacade().find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Usuarios) {
                Usuarios o = (Usuarios) object;
                return getStringKey(o.getIdUsuarios());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Usuarios.class.getName()});
                return null;
            }
        }

    }

}
