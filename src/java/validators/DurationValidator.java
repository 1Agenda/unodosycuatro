package validators;

import entidad.Lugares;
import java.util.Vector;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputtext.InputText;

@FacesValidator("durationValidator")
public class DurationValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {
        String type = value.toString();
        InputText uiInputConfirmCapcity = (InputText) component.getAttributes().get("duration");
        Object duracion = uiInputConfirmCapcity.getSubmittedValue();
        if (type == null) {
            return;
        }
        if ((Integer.parseInt(duracion + "") > 120) && (Integer.parseInt(type) == 0)) {
            throw new ValidatorException(new FacesMessage("La reunion excede de 120 minutos"));
        }

        if ((Integer.parseInt(duracion + "") <= 30) ) {
            throw new ValidatorException(new FacesMessage("La Duracion minima es de 30 Minutos"));
        }
    }

}
