package validators;

import entidad.Lugares;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;

@FacesValidator("datexValidator")
public class DatexValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        Date date = (Date) value;

        HtmlSelectOneMenu uiInputConfirmCapcity = (HtmlSelectOneMenu) component.getAttributes().get("type");
        Object duracion = uiInputConfirmCapcity.getSubmittedValue();

        if (duracion == null) {
            duracion = uiInputConfirmCapcity.getValue();
        }

        Date fecha = new Date();
        //System.out.println("yo soy la fecha" + fecha);
        //System.out.println("yo soy el tipo" + duracion);
        //System.out.println("yo soy la fecha" + date);

        Calendar cinicio = Calendar.getInstance();
        Calendar cfinal = Calendar.getInstance();

        //ESTABLECEMOS LA FECHA DEL CALENDARIO CON EL DATE GENERADO ANTERIORMENTE
        cinicio.setTime(fecha);
        cfinal.setTime(date);

        long milis1 = cinicio.getTimeInMillis();
        long milis2 = cfinal.getTimeInMillis();
        long diff = milis2 - milis1;
        long diffdias = (diff / (24 * 60 * 60 * 1000));

        //System.out.println("la diferencia es " + diffdias);

        if (diffdias < 0) {
            throw new ValidatorException(new FacesMessage("Debe Seleccionar una Fecha en el Futuro"));

        } else {

            if (Integer.parseInt(duracion.toString()) == 0 && diffdias < 2) {
                throw new ValidatorException(new FacesMessage("Las Reuniones Requieren Minimo 2 Dias de Anticipacion"));
            }


            if ((Integer.parseInt(duracion.toString()) == 1) && (date.getDay() != 1) && (diffdias >= 2)) {
                throw new ValidatorException(new FacesMessage("Los Eventos Deben Ser en lunes"));
            }
        }
    }

}
