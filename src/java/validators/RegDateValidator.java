package validators;

import entidad.Lugares;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;

@FacesValidator("regDateValidator")
public class RegDateValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        Date date = (Date) value;
        Date date3;
        Date hoy = new Date();

        HtmlSelectOneMenu typeSelector = (HtmlSelectOneMenu) component.getAttributes().get("type");
        Object type = typeSelector.getSubmittedValue();

        if (type == null) {
            type = typeSelector.getValue();
        }

        org.primefaces.component.calendar.Calendar date2 = (org.primefaces.component.calendar.Calendar) component.getAttributes().get("dateS");
        date3 = (Date) date2.getValue();

        if ((Integer.parseInt(type.toString()) == 0) && (date != null)) {
            throw new ValidatorException(new FacesMessage("Las Reuniones no pueden tener Fecha de Registro"));
        }

        if ((Integer.parseInt(type.toString()) == 1) && (date == null)) {
            throw new ValidatorException(new FacesMessage("Falta Fecha de Registro"));
        }

        try {
            //System.out.println("yo soy el tipo" + type);
            //System.out.println("yo soy la fecha" + date3);
            //System.out.println("yo soy la fecha" + date);
            //System.out.println("hoy" + hoy);

            Calendar cinicio = Calendar.getInstance();
            Calendar cfinal = Calendar.getInstance();

            cinicio.setTime(date);
            cfinal.setTime(date3);

            long milis1 = cinicio.getTimeInMillis();
            long milis2 = cfinal.getTimeInMillis();
            long diff = milis2 - milis1;
            long diffdias = (diff / (24 * 60 * 60 * 1000));

            //System.out.println("la diferencia es " + diffdias);

            if (!date3.after(date)) {
                throw new ValidatorException(new FacesMessage("La fecha de Registro Debe ser antes de La Fecha del Evento"));
            }

            if (date.getDay() != 1) {
                throw new ValidatorException(new FacesMessage("La fecha de Inicio de Registro Debe ser Lunes"));
            }

            if (hoy.after(date)) {
                throw new ValidatorException(new FacesMessage("La fecha de Registro debe ser en el Futuro"));
            }

            if (diffdias != 7) {
                throw new ValidatorException(new FacesMessage("La fecha de debe ser una semana antes del evento"));
            }
        } catch (NullPointerException e) {

        }
    }
}
