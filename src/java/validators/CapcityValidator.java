package validators;

import entidad.Lugares;
import java.util.Vector;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("capcityValidator")
public class CapcityValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        String people = value.toString();
        int capacidad = 0;

        if (Integer.parseInt(people) >= 2) {
            entidad.Lugares place = (entidad.Lugares) component.getAttributes().get("lugar");

            //System.out.println(place);

            capacidad = place.getCapcidad();

            if (Integer.parseInt(people) > capacidad) {
                throw new ValidatorException(new FacesMessage("la capacidad de " + place.getNombre()
                        + " esta excedida su capacidad maxima es de " + place.getCapcidad()));
            }
        } else {
            throw new ValidatorException(new FacesMessage("la cantidad minima de participantes es 2"));
        }

    }

}
