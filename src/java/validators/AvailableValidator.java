package validators;

import entidad.Agenda;
import entidad.Lugares;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;

@FacesValidator("availableValidator")
public class AvailableValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        List<Agenda> Agendados = (List<Agenda>) component.getAttributes().get("lugarD");
        InputText HoraInputText = (InputText) component.getAttributes().get("hora");
        InputText DuracionInputText = (InputText) component.getAttributes().get("duracion");
        Object id = (Object) component.getAttributes().get("idx");
        int idx2 = -1;
        Date temp;

        if (id != null) {
            idx2 = Integer.parseInt(id.toString());
        }
        System.out.println(idx2);

        try {
            Date hora = (Date) HoraInputText.getValue();

            //System.out.println(hora.getHours() + " " + hora.getMinutes());
            if (Agendados.size() > 0) {

                for (Agenda reunion : Agendados) {

                    if (idx2 != reunion.getIdAgenda()) {
                        if (hora.equals(reunion.getHora())) {
                            temp = addMinutesToDate(reunion.getHora(), reunion.getDuracion());
                            throw new ValidatorException(new FacesMessage("El lugar esta ocuapdo por un evento que empieza a las" + reunion.getHora().getHours() + ":" + reunion.getHora().getMinutes() + " y se desocupa a las " + temp.getHours() + ":" + temp.getMinutes()));
                        } else {
                            if (hora.after(reunion.getHora())) {
                                temp = addMinutesToDate(reunion.getHora(), reunion.getDuracion());
                                if (hora.equals(temp) || temp.after(hora)) {
                                    throw new ValidatorException(new FacesMessage("El lugar esta ocuapdo por un evento que empieza a las" + reunion.getHora().getHours() + ":" + reunion.getHora().getMinutes() + " y se desocupa a las " + temp.getHours() + ":" + temp.getMinutes()));
                                }
                            } else {
                                temp = addMinutesToDate(hora, Integer.parseInt(DuracionInputText.getValue().toString()));
                                if (reunion.getHora().equals(temp) || temp.after(reunion.getHora())) {
                                    temp = addMinutesToDate(reunion.getHora(), reunion.getDuracion());
                                    throw new ValidatorException(new FacesMessage("El lugar esta ocuapdo por un evento que empieza a las" + reunion.getHora().getHours() + ":" + reunion.getHora().getMinutes() + " y se desocupa a las " + temp.getHours() + ":" + temp.getMinutes()));
                                }
                            }
                        }
                    }

                }
            }
        } catch (NullPointerException e) {
        }

    }

    public static Date addMinutesToDate(Date date, int minutes) {
        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(date);
        calendarDate.add(Calendar.MINUTE, minutes);
        return calendarDate.getTime();
    }

    public static Date removeMinutesToDate(Date date, int minutes) {
        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(date);
        calendarDate.add(Calendar.MINUTE, (-1 * minutes));
        return calendarDate.getTime();
    }
}
