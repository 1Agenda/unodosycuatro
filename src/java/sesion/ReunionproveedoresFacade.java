/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Agenda;
import entidad.Eventoparticipantes;
import entidad.Reunionproveedores;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Daftzero
 */
@Stateless
public class ReunionproveedoresFacade extends AbstractFacade<Reunionproveedores> {

    @PersistenceContext(unitName = "WebApplication7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReunionproveedoresFacade() {
        super(Reunionproveedores.class);
    }

    public int findAllCountByIdAgenda(Agenda idAgenda) {
        int count
                = ((Long) em.createNamedQuery("Reunionproveedores.findAllCountByIdAgenda")
                .setParameter("idAgenda", idAgenda)
                .getSingleResult())
                .intValue();
        return count;
    }

    public List<Reunionproveedores> findByAgenda(entidad.Agenda agenda) {
        TypedQuery<Reunionproveedores> query = em.createNamedQuery("Reunionproveedores.findByIdAgenda", Reunionproveedores.class)
                .setParameter("idAgenda", agenda);
        return query.getResultList();
    }

}
