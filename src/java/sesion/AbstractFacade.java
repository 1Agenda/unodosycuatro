/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Daftzero
 */
public abstract class AbstractFacade<T> {
    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
    

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public List<T> findBySpecificField(String field, Object fieldContent, String predicates, LinkedHashMap<String, String> ordering,
           LinkedList<String> grouping){
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<T> root = cq.from(entityClass);
        Predicate predicate = null;
        
        if(predicates.equals("equal"))
        {
            predicate = cb.equal(root.get(field), fieldContent);
        }
        else if(predicates.equals("likelower"))
        {
            predicate = cb.like(cb.lower(root.<String>get(field)), fieldContent.toString());
        }
        else if(predicates.equals("like"))
        {
            predicate = cb.like(root.<String>get(field), "%"+fieldContent.toString()+"%");
        }
        
        cq.select(root);
        cq.where(predicate);
        
        if(ordering != null)
        {
            Set<String> set = ordering.keySet();
            List<Order> orders = new ArrayList<>();
            for(String orderingField : set)
            {
                Order order;
                if(ordering.get(orderingField).equals("ASC"))
                {
                    order = cb.asc(root.get(orderingField));
                }
                else
                {
                    order = cb.desc(root.get(orderingField));
                }
                orders.add(order);
            }
            cq.orderBy(orders);
        }
        
        if(grouping != null)
        {
            Iterator iterator = grouping.iterator();
            List<Expression> groups = new LinkedList<>();
            while(iterator.hasNext())
            {
                groups.add(root.get(iterator.next().toString()));
            }
            cq.groupBy(groups);
        }
        
        Query query = getEntityManager().createQuery(cq);
        query.setMaxResults(50000);
        
        return query.getResultList();
    }
}
