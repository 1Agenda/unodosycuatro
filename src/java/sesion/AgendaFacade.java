/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Agenda;
import entidad.Empleados;
import entidad.Lugares;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Daftzero
 */
@Stateless
public class AgendaFacade extends AbstractFacade<Agenda> {

    @PersistenceContext(unitName = "WebApplication7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AgendaFacade() {
        super(Agenda.class);
    }

    public List<Agenda> findByFechaAndLugar(Date fecha, Lugares idLugares) {
        TypedQuery<Agenda> query
                = em.createNamedQuery("Agenda.findByFechaAndLugar", Agenda.class)
                .setParameter("fecha", fecha)
                .setParameter("idlugares", idLugares);
        return query.getResultList();
    }

    public List<Agenda> findByReunionEmp(int idEmpleados) {
        TypedQuery<Agenda> query = em.createNamedQuery("Agenda.findByReunionEmp", Agenda.class)
                .setParameter("idEmpleados", idEmpleados);
        return query.getResultList();
    }
    
    public List<Agenda> findByReunionProv(int idProveedores) {
        TypedQuery<Agenda> query = em.createNamedQuery("Agenda.findByReunionProv", Agenda.class)
                .setParameter("idProveedores", idProveedores);
        return query.getResultList();
    }
       
    public  List<Agenda> findByType(int i)
    {
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByTipo", Agenda.class)
                .setParameter("tipo", i);
        return  query.getResultList();
        
    }
    
    public  List<Agenda> findByIndex(Date fecha){
        System.out.println("Impriendo fecha " + fecha);
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByConsultaF", Agenda.class)
                .setParameter("fecha", fecha)                
                .setParameter("tipo", 1);
        return query.getResultList();
        
    }
    public  List<Agenda> findByReunionEmpNombre(String nombre){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionEmpNombre", Agenda.class)
                .setParameter("nombre", nombre); 
        return query.getResultList();
        
    }
     public  List<Agenda> findByReunionEmpFecha(Date fecha){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionEmpFecha", Agenda.class)
                .setParameter("fecha", fecha); 
        return query.getResultList();
        
    }
     public  List<Agenda> findByReunionEmpHora(Date hora){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionEmpHora", Agenda.class)
                .setParameter("hora", hora); 
        return query.getResultList();      
    }
     public  List<Agenda> findByReunionEmpLugar(String lugar){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionEmpLugar", Agenda.class)
                .setParameter("lugar", lugar); 
        return query.getResultList();      
    }
     public  List<Agenda> findByReunionEmpNombreLugar(String lugar, String nombre){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionEmpNombreLugar", Agenda.class)
                .setParameter("lugar", lugar)
                .setParameter("nombre", nombre); 
        return query.getResultList();      
    }
     public  List<Agenda> findByReunionEmpFechaHora(Date fecha, Date hora){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionEmpFechaHora", Agenda.class)
                .setParameter("fecha", fecha)
                .setParameter("hora", hora); 
        return query.getResultList();      
    }

     
     public  List<Agenda> findByReunionProvNombre(String nombre){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionProvNombre", Agenda.class)
                .setParameter("nombre", nombre); 
        return query.getResultList();
        
    }
     public  List<Agenda> findByReunionProvFecha(Date fecha){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionProvFecha", Agenda.class)
                .setParameter("fecha", fecha); 
        return query.getResultList();
        
    }
     public  List<Agenda> findByReunionProvHora(Date hora){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionProvHora", Agenda.class)
                .setParameter("hora", hora); 
        return query.getResultList();      
    }
     public  List<Agenda> findByReunionProvLugar(String lugar){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionProvLugar", Agenda.class)
                .setParameter("lugar", lugar); 
        return query.getResultList();      
    }
     public  List<Agenda> findByReunionProvNombreLugar(String lugar, String nombre){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionProvNombreLugar", Agenda.class)
                .setParameter("lugar", lugar)
                .setParameter("nombre", nombre); 
        return query.getResultList();      
    }
     public  List<Agenda> findByReunionProvFechaHora(Date fecha, Date hora){
        TypedQuery<Agenda> query=em.createNamedQuery("Agenda.findByReunionProvFechaHora", Agenda.class)
                .setParameter("fecha", fecha)
                .setParameter("hora", hora); 
        return query.getResultList();      
    }
}
