/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Agenda;
import entidad.Proveedores;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Daftzero
 */
@Stateless
public class ProveedoresFacade extends AbstractFacade<Proveedores> {

    @PersistenceContext(unitName = "WebApplication7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProveedoresFacade() {
        super(Proveedores.class);
    }

    public DualListModel<Proveedores> findAllByIdAgenda(Agenda idAgenda) {
        DualListModel<Proveedores> proveedores = new DualListModel<Proveedores>();

        try {
            System.out.println("AGENDA: " + idAgenda);
            TypedQuery query = em.createNamedQuery("Proveedores.findAllByIdAgenda", Proveedores.class)
                    .setParameter("idAgenda", idAgenda);
            List<Proveedores> proveedoresSource = query.getResultList();
            List<Proveedores> proveedoresTarget = new ArrayList<Proveedores>();

            proveedores = new DualListModel<Proveedores>(proveedoresSource, proveedoresTarget);
        } catch (NullPointerException e) {

        }

        return proveedores;

    }

}
