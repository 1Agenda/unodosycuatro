/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Agenda;
import entidad.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Daftzero
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> {
    @PersistenceContext(unitName = "WebApplication7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }
    
        public List<Usuarios> findByUserPasss(String pass, String user) {
            System.out.println("Imprimeido user " + user );
            System.out.println("Impriemndo password " + pass);
        TypedQuery<Usuarios> query = em.createNamedQuery("Usuarios.findBayPassUser", Usuarios.class)
                .setParameter("password", pass)
                .setParameter("user", user);
        
        return query.getResultList();
    }
}
