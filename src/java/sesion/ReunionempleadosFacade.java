/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Agenda;
import entidad.Reunionempleados;
import entidad.Reunionproveedores;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Daftzero
 */
@Stateless
public class ReunionempleadosFacade extends AbstractFacade<Reunionempleados> {

    @PersistenceContext(unitName = "WebApplication7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReunionempleadosFacade() {
        super(Reunionempleados.class);

    }

    public int findAllCountByIdAgenda(Agenda idAgenda) {
        int count
                = ((Long) em.createNamedQuery("Reunionempleados.findAllCountByIdAgenda")
                .setParameter("idAgenda", idAgenda)
                .getSingleResult())
                .intValue();
        return count;
    }
    
        public List<Reunionempleados> findByAgenda(entidad.Agenda agenda) {
        TypedQuery<Reunionempleados> query = em.createNamedQuery("Reunionempleados.findByIdAgenda", Reunionempleados.class)
                .setParameter("idAgenda", agenda);
        return query.getResultList();
    }

}
