/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Agenda;
import entidad.Eventoparticipantes;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Daftzero
 */
@Stateless
public class EventoparticipantesFacade extends AbstractFacade<Eventoparticipantes> {

    @PersistenceContext(unitName = "WebApplication7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EventoparticipantesFacade() {
        super(Eventoparticipantes.class);
    }

    public List<Eventoparticipantes> findByConfirmadoAgenda(boolean conf, entidad.Agenda agenda) {
        TypedQuery<Eventoparticipantes> query = em.createNamedQuery("Eventoparticipantes.findByConfirmadoIdAgenda", Eventoparticipantes.class)
                .setParameter("confirmado", conf)
                .setParameter("idAgenda", agenda);

        return query.getResultList();
    }

}
